﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SISCOClients.Models;

namespace SISCOClients.Controllers
{
    public class HomeController : Controller
    {
        private SalesforceDb db = new SalesforceDb();

        // GET: /Home/
        public ActionResult Index()
        {
            return View(new List<v_SISCOClientAccount>());
        }

        [HttpPost]
        public ActionResult Index(string name, string number)
        {
            var model = db.v_SISCOClientAccount.AsQueryable();
            if (!string.IsNullOrEmpty(name))
            {
                model = model.Where(n => n.Client.Contains(name));
            }
            if (!string.IsNullOrEmpty(number))
            {
                model = model.Where(n => n.TPARef_ == number);
            }
            if (string.IsNullOrEmpty(number) && string.IsNullOrEmpty(name))
            {
                model = model.Take(0);
            }

            ViewBag.name = name;
            ViewBag.number = number;
            return View(model.ToList());
        }

        // GET: /Home/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            v_SISCOClientAccount account = db.v_SISCOClientAccount.Find(id);
            
            if (account == null)
            {
                return HttpNotFound();
            }
            return View(account);
        }
    }
}
