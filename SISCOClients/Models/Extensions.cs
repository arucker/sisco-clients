﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace SISCOClients.Models
{
    public partial class v_SISCOClientService__c
    {
        [DisplayName("Term Begins")]
        public string ShortStartDate { get { return Term_Begins.Value.ToShortDateString(); } }

        [DisplayName("Term Ends")]
        public string ShortEndDate { get { return Term_Ends.Value.ToShortDateString(); } }
    }
    public partial class v_SISCOClientAccount
    {
        [DisplayName("Manager")]
        public string BenefitsManager { get { return BenMgr; } }

        [DisplayName("TPA #")]
        public string TPAReferenceNumber { get { return TPARef_; } }

        [DisplayName("Website")]
        public string URL
        {
            get
            {
                if (Website.Contains("http://"))
                {
                    return Website;
                }
                else
                {
                    return "http://" + Website;
                }
            }
        }
    }
    public partial class v_SISCOClientContact
    {
        [DisplayName("Name")]
        public string FullName { get { return FirstName + " " + LastName; } }
    }
    public partial class v_SISCOClientTPA_Contact__c
    {
        [DisplayName("Other Info")]
        public string OtherInfo { get { return Other_Info; } }
    }
}