//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SISCOClients.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class v_SISCOClientService__c
    {
        public string Id { get; set; }
        public string Account__c { get; set; }
        public string Anniversary { get; set; }
        public string Coverage { get; set; }
        public string Services { get; set; }
        public Nullable<System.DateTime> Term_Begins { get; set; }
        public Nullable<System.DateTime> Term_Ends { get; set; }
    
        public virtual v_SISCOClientAccount Account { get; set; }
    }
}
